import React from "react";
import Routes from "./components/primary_pages/Routes";
import Navigation from "./components/common/Navigation";
import Footer from "./components/common/Footer";
import './assets/stylesheets/common.css';

function App() {
  return (
    <>
      <Navigation />
      <div className="container">
        <Routes />
      </div>
      <Footer />
    </>
  );
}

export default App;
