import React from "react";
import { motion } from "framer-motion";
import { Helmet } from "react-helmet";

export default function Home(props) {
  return (
    <motion.div
      initial="initial"
      animate="in"
      exit="out"
      variants={props.animation_variant}
    >
      <Helmet>
        <title>Signboard.com.bd - Home</title>
        <meta name="description" content="This is Signboard.com.bd Homepage Description." />
      </Helmet>
      <h1>Home</h1>
      <p>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Soluta ut nulla
        quidem voluptatibus sequi, molestiae nesciunt nihil omnis hic culpa.
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Soluta ut nulla
        quidem voluptatibus sequi, molestiae nesciunt nihil omnis hic culpa.
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Soluta ut nulla
        quidem voluptatibus sequi, molestiae nesciunt nihil omnis hic culpa.
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Soluta ut nulla
        quidem voluptatibus sequi, molestiae nesciunt nihil omnis hic culpa.
      </p>
      <p>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Soluta ut nulla
        quidem voluptatibus sequi, molestiae nesciunt nihil omnis hic culpa.
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Soluta ut nulla
        quidem voluptatibus sequi, molestiae nesciunt nihil omnis hic culpa.
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Soluta ut nulla
        quidem voluptatibus sequi, molestiae nesciunt nihil omnis hic culpa.
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Soluta ut nulla
        quidem voluptatibus sequi, molestiae nesciunt nihil omnis hic culpa.
      </p>
      <p>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Soluta ut nulla
        quidem voluptatibus sequi, molestiae nesciunt nihil omnis hic culpa.
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Soluta ut nulla
        quidem voluptatibus sequi, molestiae nesciunt nihil omnis hic culpa.
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Soluta ut nulla
        quidem voluptatibus sequi, molestiae nesciunt nihil omnis hic culpa.
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Soluta ut nulla
        quidem voluptatibus sequi, molestiae nesciunt nihil omnis hic culpa.
      </p>
      <p>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Soluta ut nulla
        quidem voluptatibus sequi, molestiae nesciunt nihil omnis hic culpa.
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Soluta ut nulla
        quidem voluptatibus sequi, molestiae nesciunt nihil omnis hic culpa.
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Soluta ut nulla
        quidem voluptatibus sequi, molestiae nesciunt nihil omnis hic culpa.
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Soluta ut nulla
        quidem voluptatibus sequi, molestiae nesciunt nihil omnis hic culpa.
      </p>
    </motion.div>
  );
}
