import React from "react";
import { Switch, Route } from "react-router-dom";
import Home from "./Home";
import About from "./About";
import HeroBlockOne from "../custom_components/HeroBlockOne";
import { AnimatePresence } from "framer-motion";
import BlogDetails from "../Blogs/BlogDetails";
// import HeroBlockOneDummy from "../custom_components/HeroBlockOneDummy";

export default function Routes() {
  const pageVariants = {
    initial: {
      y: "50px",
    },
    in: {
      y: 0,
    },
    out: {
      y: "50px",
    },
  };

  const api_options = {
    per_page: 8,
    order: "desc",
  };

  return (
    <>
      <AnimatePresence exitBeforeEnter>
        <Switch>
          <Route path="/" exact>
            <Home animation_variant={pageVariants} />
          </Route>
          <Route path="/about" exact>
            <About />
          </Route>
          <Route path="/development-blogs" exact>
            <HeroBlockOne
              api_url="http://localhost/wordpress_api/wp-json/wp/v2/posts"
              api_options={api_options}
              animation_variant={pageVariants}
            />
          </Route>
          <Route path="/details/:id/:slug" exact>
            <BlogDetails/>
          </Route>
        </Switch>
      </AnimatePresence>
    </>
  );
}
