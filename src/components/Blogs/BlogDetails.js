import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import CircularProgress from "@material-ui/core/CircularProgress";
import placeholder from "../../assets/image/placeholder.jpg";
import axios from "axios";

export default function BlogDetails() {
  const { id, slug } = useParams();

  const [apiCalling, setApiCalling] = useState(true);
  const [details, setDetails] = useState([]);
  useEffect(() => {
    axios
    .get("http://localhost/wordpress_api/wp-json/wp/v2/posts", {
      headers: {
        Accept: "application/json",
      },
      params: {
        slug: slug,
      },
    })
    .then((response) => {
      setDetails(response.data[0]);
    })
    .catch((error) => {
      console.log("BlockDetails.js", error);
    })
    .then(() => {
      setApiCalling(false);
    });
  },[]);


  if (apiCalling) {
    return (
      <>
        <div
          style={{
            display: "flex",
            width: "100%",
            height: "200px",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <CircularProgress />
        </div>
      </>
    );
  }

  return (
      <>
          <img
            src={!details.fimg_url ? placeholder : details.fimg_url}
        style={{ width: "100%" }}
            alt=""
          />
          <h1>{ details.title.rendered}</h1>
    </>
  );
}
