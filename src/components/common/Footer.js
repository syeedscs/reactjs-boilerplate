import React from "react";
import { Link } from "react-router-dom";

export default function Footer() {
  return (
    <>
      <section className="footer">
        <div className="widgets">
          <div className="widget">
            <h4>Signboard.com.bd</h4>
            <div>
              <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsum,
                molestias voluptate dolorem officiis incidunt ab!
              </p>
              <p>
                <i className="fa fa-envelope"></i> info@signboard.com.bd
              </p>
              <p>
                <i className="fa fa-headset"></i> +8801X-XXXX-XXXX
              </p>
            </div>
          </div>
          <div className="widget">
            <h4><i className="fas fa-question-circle"></i>&nbsp;General Information</h4>
            <ul>
              <li>
                <Link to="/"><i className="fas fa-home"></i>&nbsp;Home</Link>
              </li>
              <li>
                <Link to="/about-us"><i className="fas fa-info-circle"></i>&nbsp;About Us</Link>
              </li>
              <li>
                <Link to="/terms-and-conditions"><i className="fas fa-file-contract"></i>&nbsp;Terms & Conditions</Link>
              </li>
              <li>
                <Link to="/privacy-policy"><i className="fas fa-user-secret"></i>&nbsp;Privacy Policy</Link>
              </li>
            </ul>
          </div>
          <div className="widget">
            <h4>Widget Header</h4>
            <ul>
              <li>
                <Link to="/">Lorem, ipsum dolor</Link>
              </li>
              <li>
                <Link to="/">Lorem, ipsum dolor</Link>
              </li>
              <li>
                <Link to="/">Lorem, ipsum dolor</Link>
              </li>
              <li>
                <Link to="/">Lorem, ipsum dolor</Link>
              </li>
            </ul>
          </div>
          <div className="widget">
            <h4>
              <i className="fas fa-feather-alt"></i>&nbsp;Blogs
            </h4>
            <ul>
              <li>
                <Link to="/development-blogs">
                  <i className="fas fa-code"></i> Development Blogs
                </Link>
              </li>
              <li>
                <Link to="/">Lorem, ipsum dolor</Link>
              </li>
              <li>
                <Link to="/">Lorem, ipsum dolor</Link>
              </li>
              <li>
                <Link to="/">Lorem, ipsum dolor</Link>
              </li>
            </ul>
          </div>
        </div>
        <div className="bottom-bar">
          Copyright&nbsp;<i className="far fa-copyright"></i>&nbsp;
          {new Date().getFullYear()}
        </div>
      </section>
    </>
  );
}
