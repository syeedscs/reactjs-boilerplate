import React from "react";
import { Link } from "react-router-dom";
import '../../assets/stylesheets/navigation.css'

export default function Navigation() {
  const handleClick = () => {
    document.querySelector(".search-bar div").classList.toggle("show");
    document.querySelector(".search-bar div input").focus();
  };
  return (
    <>
      <section className="header">
        <nav className="top-nav">
          <ul className="language-selector">
            <li>বাংলা</li>
            <li className="active">English</li>
          </ul>
        </nav>
        <nav className="navigation">
          <div className="menu">
            <ul className="submenu">
              <li>
                <i className="fas fa-bars"></i>
              </li>
            </ul>
          </div>
          <div className="logo">signboard</div>
          <div className="menu">
            <ul className="main-menu">
              {/* <li>
                <a href="/#">Home</a>
              </li>
              <li>
                <a href="/#">About</a>
              </li> */}
            </ul>
            <ul className="submenu">
              <Link to="/">
                <li>
                  <i className="fas fa-home"></i>
                </li>
              </Link>
              <li onClick={handleClick} className="search-button">
                <i className="fas fa-search"></i>
              </li>
              <li>
                <i className="fas fa-user-circle"></i>
              </li>
              {/* <li>
                <i className="fas fa-sign-in-alt"></i>
              </li> */}
            </ul>
          </div>
        </nav>
      </section>
      <section className="search-bar">
        <div>
          <input placeholder="Search Product, Save Money" />
        </div>
      </section>
    </>
  );
}
