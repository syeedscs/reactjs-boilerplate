import React from "react";
import placeholder from "../../assets/image/placeholder.jpg";
import { Link } from "react-router-dom";

export default function PostBlock(props) {
  return (
    <>
      <Link to={`/details/${props.post.id}/${props.post.slug}`}>
        <div className="hero" style={{height:"100%"}}>
          <img
            src={!props.post.fimg_url ? placeholder : props.post.fimg_url}
            style={{ width: "100%" }}
            alt=""
          />
          <h3>{props.post.title.rendered}</h3>
        </div>
      </Link>
    </>
  );
}
