import { motion } from "framer-motion";
import React, { useEffect, useState } from "react";
import axios from "axios";
import PostBlock from "./PostBlock";
import CircularProgress from "@material-ui/core/CircularProgress";

export default function HeroBlockOne(props) {
  const [posts, setPosts] = useState([]);
  const [apiCalling, setApiCalling] = useState(true);

  const call_api = () => {
    axios
      .get(props.api_url, {
        headers: {
          Accept: "application/json",
        },
        params: props.api_options,
      })
      .then((response) => {
        setPosts(response.data);
      })
      .catch((error) => {
        console.log("hero block one", error);
      })
      .then(() => {
        setApiCalling(false);
      });
  };

  useEffect(() => {
    call_api();
  }, []);

  if (apiCalling) {
    return (
      <>
        <div
          style={{
            display: "flex",
            width: "100%",
            height: "200px",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <CircularProgress/>
        </div>
      </>
    );
  }
  return (
    <motion.div
      initial="initial"
      animate="in"
      exit="out"
      variants={props.animation_variant}
    >
      <section className="hero-block-1">
        <h1>Entertainment</h1>
        <div className="blocks">
          {posts.map((post) => (
            <PostBlock key={post.id} post={post} />
          ))}
        </div>
      </section>
    </motion.div>
  );
}
