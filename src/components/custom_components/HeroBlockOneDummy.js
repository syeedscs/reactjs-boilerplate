import { motion } from "framer-motion";
import React from "react";

export default function HeroBlockOneDummy(props) {
  return (
    <motion.div
      initial="initial"
      animate="in"
      exit="out"
      variants={props.animation_variant}
    >
      <section className="hero-block-1">
        <h1>Entertainment</h1>
        <div className="blocks">
          <div className="hero">
            <img
              src="https://picsum.photos/640/480"
              style={{ width: "100%" }}
              alt=""
            />
            <h3>টেস্ট পোস্ট ১</h3>
            <p>
              টেস্ট পোস্ট ১ টেস্ট পোস্ট ১ টেস্ট পোস্ট ১ টেস্ট পোস্ট ১ টেস্ট
              পোস্ট ১ টেস্ট পোস্ট ১
            </p>
          </div>
          <div className="hero">
            <img
              src="https://picsum.photos/643/480"
              style={{ width: "100%" }}
              alt=""
            />
            <h3>Title</h3>
            <p>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Suscipit
              magnam dicta quidem veniam. Unde dolorem modi atque iure officiis
              dicta.
            </p>
          </div>
          <div className="hero">
            <img
              src="https://picsum.photos/641/480"
              style={{ width: "100%" }}
              alt=""
            />
            <h3>Title</h3>
            <p>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Suscipit
              magnam dicta quidem veniam. Unde dolorem modi atque iure officiis
              dicta.
            </p>
          </div>
          <div className="hero">
            <img
              src="https://picsum.photos/642/480"
              style={{ width: "100%" }}
              alt=""
            />
            <h3>Title</h3>
            <p>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Suscipit
              magnam dicta quidem veniam. Unde dolorem modi atque iure officiis
              dicta.
            </p>
          </div>
        </div>
      </section>
    </motion.div>
  );
}
